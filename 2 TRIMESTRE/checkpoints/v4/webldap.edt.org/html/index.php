<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>WEB LDAP</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>

  <header>
      <h1 class="tittle"> PROJECTE 2N TRIMESTRE </h1>
      <center>
            <a href="http://web.edt.org/index.html"><img src="images/logo.png" width="30%"></a>
      </center>
  </header>
  <br>

  <article>
    <br>
    <a href="http://webldap.edt.org/index.php" style="font-size: 35px; margin-right: 50px">LDAP</a>
    <a href="https://webapirest.edt.org/index.html"style="font-size: 35px;">APIREST</a>
    <br><br> 
  </article>

  <br>

    <article>
      <form method="POST" action="">
        <h1><strong><label for="sql">FES LA TEVA CONSULTA SQL</label></strong></h1>
		    <textarea id="sql" name="sql" rows="3" cols="125"></textarea>
		    <br><br>
		    <input type="submit" value="Consultar">
        <br><br>
	    </form>

      <?php
      // Conexión a la base de datos training
      $dbconn = pg_connect("host=psql.edt.org dbname=training user=prova password=passwd") or die('No se pudo conectar: ' . pg_last_error());
      
      // si se ingresa un valor en el formulario 
      if (isset($_POST["sql"])) {
          // Sentencia SQL para extraer los datos que deseamos mostrar
          $query = $_POST["sql"];
          $result = pg_query($dbconn, $query) or die('La consulta falló: ' . pg_last_error());

          // Mostrar los resultados en una tabla HTML
          echo "<table>";
          echo "<tr>";
          // Creamos dinámicamente los encabezados segun el número de campos
          for ($i = 0; $i < pg_num_fields($result); $i++) {
              echo "<th>" . pg_field_name($result, $i) . "</th>";
          }
          echo "</tr>";

          // pg_fetch_assoc() retorna una matriz asociativa a los datos obtenidos con la sentencia SQL
          while ($row = pg_fetch_assoc($result)) {
              echo "<tr>";
              // Por cada fila, mostramos datos 
              foreach ($row as $field) {
                  echo "<td>" . $field . "</td>";
              }
              echo "</tr>";
          }
          echo "</table>";
      }
      // Cerrar la conexión a la base de datos
      pg_close($dbconn);
      ?>
    </article>

    <br>

    <article>
      <br>
      <strong style="font-size: 15px; margin-right: 10px; list-style-type: none;"><a href="#TAULA CLIENTS">TAULA CLIENTS</a></strong>
      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA PRODUCTES">TAULA PRODUCTES</a></strong>
      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA COMANDES">TAULA COMANDES</a></strong>
      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA OFICINES">TAULA OFICINES</a></strong>
      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA REPVENTAS">TAULA REPVENTAS</a></strong>
      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA RELACIONAL">TAULA RELACIONAL</a></strong>
      <br><br>
    </article>

    <br>

    <article id="TAULA CLIENTS">
      <u><h1>TAULA CLIENTS</h1></u>
      <center>
        <img src="images/clientes.png">
      </center>
      <br>
    </article>

    <br>

    <article id="TAULA PRODUCTES">
      <u><h1>TAULA PRODUCTES</h1></u>
      <center>
        <img src="images/productos.png">
      </center>
      <br>
    </article>

    <br>

    <article id="TAULA COMANDES">
      <u><h1>TAULA COMANDES</h1></u>
      <center>
        <img src="images/pedidos.png">
      </center>
      <br>
    </article>

    <br>

    <article id="TAULA OFICINES">
      <u><h1>TAULA OFICINES</h1></u>
      <center>
        <img src="images/oficinas.png">
      </center>
      <br>
    </article>

    <br>

    <article id="TAULA REPVENTAS">
      <u><h1>TAULA REPVENTAS</h1></u>
      <center>
        <img src="images/repventas.png">
      </center>
      <br>
    </article>

    <br>

    <article id="TAULA RELACIONAL">
      <u><h1>TAULA RELACIONAL</h1></u>
      <center>
        <img src="images/funcionament.png">
      </center>
      <br>
    </article>

    <br>

  </body>

  <footer>
    <u>
    <br>
    <strong> Autors: Maria Marlene Flor Benitez,  Kevin Gabriel Gonzalez Trujillo, Bruno Rodríguez Aranibar i Lucas Rodríguez Cabañeros </strong>
    <br>
    <strong>Gitlab: <a href="https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE">https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE</strong></a>
    <br>
    <strong>DockerHub: <a href="https://hub.docker.com/u/brunora">https://hub.docker.com/u/brunora</strong></a>
    <br>
    <strong> All rights deserved: 2HISX SL </strong>
    <br><br>
    </u>
  </footer>

</html>
