# __PROJECTE TRIMESTRAL__
# __Segon Trimestre__
## __Integrants__
### Bruno Rodríguez
### Kevin Gonzalez
### Lucas Rodríguez
### Marlene Flor

# ÍNDEX

# 1. Esquema de funcionament

 ![esquema](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/Esquema_proyecto_2do_trimestre.png)

# 2. Desplegament 
## 2.1 Docker Compose

![compose](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/compose.png)

## 2.2 Terraform

![terraform1](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/terraform1.png)
![terraform2](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/terraform2.png)
![terraform3](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/terraform3.png)

# 3. Servidor web
## 3.1 web.edt.org
### - Estils

## 3.2 webldap.edt.org
### - Autenticació

![ldapauth](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/ldap.png)

### - Pàgina dinàmica

![codiphp](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/codi-php.png)

## 3.3 webapirest.edt.org
### - SSL

![sslcertificate](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/ssl.png)

### - APIREST

![esquemaAPI](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/apirest_esq.png)

### - OAuth

![oauth](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/oauth.png)
