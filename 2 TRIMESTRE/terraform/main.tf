
terraform {
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
}
}

provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWXBWMF3NN"
  secret_key = "Josg0ixgQxOuap+JJne/lXN90oVFi3cjLd/ZfKEK"
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.example.id}"
  allocation_id = "eipalloc-0180d32aeba990d61"
}

resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.small" // replace with your desired instance type
  key_name = "apache"
  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
  tags = {
    Name = "docker-instance"
  }
}


resource "null_resource" "copy_script" {
  depends_on = [aws_instance.example]

  provisioner "file" {
    source      = "./install_docker.sh"
    destination = "/home/admin/install_docker.sh"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
resource "null_resource" "copy_yaml" {
  depends_on = [null_resource.copy_script]

  provisioner "file" {
    source      = "./compose.yml"
    destination = "/home/admin/compose.yml"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
resource "null_resource" "run_script" {
  depends_on = [null_resource.copy_yaml]

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/install_docker.sh",
      "bash /home/admin/install_docker.sh",
      "sudo docker compose -f /home/admin/compose.yml up -d"
    ]

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}

resource "aws_security_group" "webserver-sg" {
  name = "webserver-sg"
  description = "Puertos para funcionamiento del servidor web"

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "psql"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "spotify apirest"
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ldap"
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// Expose the EC2 instance's public IP address
output "public_ip" {
  value = "18.133.60.118"
}
