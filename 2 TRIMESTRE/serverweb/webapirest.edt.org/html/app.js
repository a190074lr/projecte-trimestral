/**
 * This is an example of a basic node.js script that performs
 * the Authorization Code oAuth2 flow to authenticate against
 * the Spotify Accounts.
 *
 * For more information, read
 * https://developer.spotify.com/web-api/authorization-guide/#authorization_code_flow
 */

var express = require('express'); // Express web server framework
var request = require('request'); // "Request" library
var cors = require('cors');
var querystring = require('querystring');
var cookieParser = require('cookie-parser');

var client_id = '7bc337b5935e4113af53be16155d343a'; // Your client id
var client_secret = 'bca6440f030f4e4b8d210dd1ef8adf71'; // Your secret
var redirect_uri = 'https://localhost:8888/callback'; // Your redirect uri


/** HTTP TO HTTPS  */
var https = require('https');
var fs = require('fs');

var privateKey = fs.readFileSync('/etc/ssl/private/webapirest.key');
var certificate = fs.readFileSync('/etc/ssl/certs/webapirest.crt');

var options = {
  key: privateKey,
  cert: certificate
};

/** */

/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var stateKey = 'spotify_auth_state';

var app = express();

app.use(express.static(__dirname + '/public'))
   .use(cors({
      origin: 'https://webapirest.edt.org:8888',
      credentials: true
   }))
   .use(cookieParser());
// maneja la solicitud de inicio de sesión 

app.get('/login', function(req, res) {

  var state = generateRandomString(16);
  res.cookie(stateKey, state, {
    maxAge: 10000,
  });
  console.log(state); 

  // your application requests authorization
  var scope = 'user-read-private user-read-email';
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: scope,
      redirect_uri: redirect_uri,
      state: state
    }));
});

//  este bloque maneja  la respuesta de la página de autorización de Spotify
app.get('/callback', function(req, res) {

  // your application requests refresh and access tokens
  // after checking the state parameter

  var code = req.query.code || null;
  var state = req.query.state || null;
  var storedState = req.cookies ? req.cookies[stateKey] : null;
  console.log('State cookie set to:', storedState);
  console.log("code:", code);
  console.log("state:", state);
  console.log("storedState:", storedState);


  if (state === null || state !== storedState) {
    res.redirect('/#' +
      querystring.stringify({
        error: 'state_mismatch'
      }));
  } else {
    console.log('State cookie value:', storedState); // log the state cookie value
    res.clearCookie(stateKey);
    console.log('State cookie cleared'); // log that the state cookie has been cleared
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true,
      credentials: 'include'
    };

    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {

        var access_token = body.access_token,
            refresh_token = body.refresh_token;

        var options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + access_token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
          console.log(body);
        });

        // we can also pass the token to the browser to make requests from there
        res.redirect('/#' +
          querystring.stringify({
            access_token: access_token,
            refresh_token: refresh_token
          }));
      } else {
        res.redirect('/#' +
          querystring.stringify({
            error: 'invalid_token'
          }));
      }
    });
  }
});


// este bloque maneja la solicitud de actualización del token de acceso. 
//
app.get('/refresh_token', function(req, res) {

  // requesting access token from refresh token
  var refresh_token = req.query.refresh_token;
  var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
    form: {
      grant_type: 'refresh_token',
      refresh_token: refresh_token
    },
    json: true
  };

  request.post(authOptions, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      res.send({
        'access_token': access_token
      });
    }
  });
});

// console.log('Listening on 8888');
// app.listen(8888);


https.createServer(options, app).listen(8888, function() {
  console.log('Listening on port 8888 (HTTPS)');
});
