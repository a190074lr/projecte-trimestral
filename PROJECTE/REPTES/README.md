#	LOCALHOST LDAP AUTENTICHATE


## CONFIGURACIÓ SERVEI ( LOCALHOST )

### CONFIGURACIÓ STARTUP.SH

Configurar el startup.up per tal de que el primer cop que encem el ldap crei la BD, i els següents cops tingui persisténcia de dades i no "planxi" de nou la BD.

> ```
> FILE=/var/lib/ldap/.bdcreat
>
> if [ -f "$FILE" ]; then
>	      /usr/sbin/slapd -d0
>
> else
>	      echo "Inicialització BD ldap edt.org"
>	      rm -rf /etc/ldap/slapd.d/*
>	      rm -rf /var/lib/ldap/*
>	      slaptest -f slapd.conf -F /etc/ldap/slapd.d
>	      slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
>	      chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
>	      touch /var/lib/ldap/.bdcreat
>	      /usr/sbin/slapd -d0
> fi
> exit 0
> ```

> __Fem el touch per tal de que el primer cop que s'inicia el servidor crear un fitxer, per tal de que el següent cop que s'inicia comprova si existeix aquest fitxer.__

>> *Si existeix no crea de nou la BD.*
>> *Si no exixsteix crea la BD.*

### ACTUALITZAR DOCKER HUB

1. Crear la nova imatge amb el startup.sh __actualitzat__.

> ```
> $ docker build -t a190074lr/m14_projectes:fase1
> ```

2. Actualitzar __docker hub.__

> ```
> $ docker push a190074lr/m14_projectes:fase1
> ```

### EDITAR EL COMPOSE

> ```
> version: "2"
> services:
>   ldap:
>       image: a190074lr/m14_projectes:fase1
>       container_name: ldap.edt.org
>       hostname: ldap.edt.org
>       ports:
>         - "389:389"
>       volumes:
>         - ldap-config:/etc/ldap/slapd.d
>         - ldap-data:/var/lib/ldap
>       networks:
>         - 2hisx
>     phpldapadmin:
>       image: a190074lr/ldap22:phpldapadmin
>       container_name: phpldapadmin.edt.org
>       hostname: phpldapadmin.edt.org
>       ports:
>         - "80:80"
>       networks:
>         - 2hisx
>  networks:
>    2hisx:
>  volumes:
>    ldap-config:
>    ldap-data:
> ```

### DESPLEGAR EL COMPOSE

> ```
> $ docker compose up -d
> ```

## INSTALACIÓ DE PAQUETS NECESSARI ( CLIENT )

```
$ apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
```

## EDICIÓ DE FITXERS ( ORDINADOR REAL )

__Editar el fitxer `/etc/ldap/ldap.conf`__.

> ```
> BASE	dc=edt,dc=org
> URI	ldap://ldap.edt.org
> ```

__Editar el fitxer `/etc/hosts`__.

> ```
> 54.164.136.205	ldap.edt.org
> ```

__Editar el fitxer `/etc/nsswicht.conf`__.

> ```
> passwd:         files ldap
> group:          files ldap
> ```

__Editar el fitxer `/etc/nslcd.conf`__.

> ```
> uri ldap://ldap.edt.org
> base dc=edt,dc=org
> ```

__Editar el fitxer `/etc/pam.d/common-session` -> Per la creació del home dir.__

> ```
> session	required	pam_unix.so 
> session optional	pam_mkhomedir.so
> session	optional	pam_mount.so 
> session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
> ```

## REINICIAR ELS SERVIDORS ( ORDINADOR REAL )

__Reiniciar el servidor `nsswicht.service`__.

> ```
> $ systemctl restart nsswicht.service
> ```

__Reiniciar el servidor `nslcd.service`__.

> ```
> $ systemctl restart nscld.service
> ```

## CREACIÓ USUARIS UNIX ( ORDINADOR REAL )

Utilitzem el script per tal de crear-hi els usuaris unix, __SENSE LA CREACIÓ DE DIR HOME.__

> ```
> for user in unix01 unix02 unix03 unix04 unix05
> do
>	useradd -s /bin/bash $user
>	echo -e "$user\n$user" | passwd $user
> done
> ```

## COMPROVACIONS ( ORDINADOR REAL )

__1r.__ Comprovacions users ldap.

> ````
> $ login marta
> Password: 
> Linux g12 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64
>
> The programs included with the Debian GNU/Linux system are free software;
> the exact distribution terms for each program are described in the
> individual files in /usr/share/doc/*/copyright.
> 
> Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
> permitted by applicable law.
> Creating directory '/tmp/home/marta'.
> ```

__2n.__ Comprovacions users unix.

> ```
> $ login unix02
> Password: 
> Linux g12 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64
>
> The programs included with the Debian GNU/Linux system are free software;
> the exact distribution terms for each program are described in the
> individual files in /usr/share/doc/*/copyright.
>
> Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
> permitted by applicable law.
> Last login: Wed Nov 23 12:01:19 CET 2022 on pts/0
> ```

## CONTAINER PHPLDAPADMIN

Encendre el container.

> ```
> $ docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d a190074lr/ldap22:phpldapadmin
> ```docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/ldap22:base