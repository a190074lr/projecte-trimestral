# !/bin/bash
docker network create 2hisx
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:ldap
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d marleneflor/projecte1t:phpldapadmin
docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx --privileged -d marleneflor/ssh22:base
docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -it marleneflor/samba22:base /bin/bash
