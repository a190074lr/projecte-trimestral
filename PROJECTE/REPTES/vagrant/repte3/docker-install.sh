# !/bin/bash
sudo apk add docker
sudo addgroup vagrant docker
rc-update add docker boot
service docker start
# install python3 and pip
sudo apk add --update python3 --no-cache py3-pip
# install required dependencies
sudo apk add gcc musl-dev libffi-dev python3-dev
sudo pip3 install cffi && sudo pip3 install --upgrade pip
# install docker-compose
sudo pip3 install docker-compose && docker-compose --version
