## REPTE 6

### SSH 
```
ssh -i ansible_key ansible@192.168.56.10

ssh-keygen -f "/home/astro/.ssh/known_hosts" -R "192.168.56.10"
```
### PLAYBOOK
```
ansible-playbook -u ansible --private-key ansible_key -i inventory_lab_public.yaml playbook.yaml

```
### Para una tarea especifica: 
```
ansible-playbook -u ansible --private-key ansible_key -i inventory_lab_public.yaml --start-at-task="task_name" playbook.yaml
```

### TEST

ERROR: Al intentar lanzar un container, error No module docker (Docker SDK python library) y ansible_python_interpreter.   
SOLUCION: Definimos python3 como interprete en ansible y instalamos con **pip** Docker SDK para python   

```
pip:
    name: docker
vars:
    ansible_python_interpreter: /usr/bin/python3
```

###### ¿Qué es Docker SDK para Python?

Una libreria de Python para la API de Docker Engine.Le permite hacer todo lo que hace el comando docker, pero desde las aplicaciones de Python: run contenedores, administrar contenedores, etc.
