> ### 1.3 SERVEI PHPLDAPADMIN

>> Desplegar al cloud usant el mateix fitxer compose.yml un servidor phpldapadmin que permet accedir a l’administració del servidor LDAP.

>>> #### 1.3.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/ADMIN*

>>> #### 1.3.2 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei PHPLDAPADMIN.

>>>> ```
>>>> #! /bin/bash
>>>> bash /opt/docker/install.sh
>>>> /sbin/php-fpm
>>>> /usr/sbin/httpd -D FOREGROUND
>>>> #httpd -S
>>>> #echo $(pgrep httpd)
>>>> ```

>>> #### 1.3.3 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # phpldapadmin
>>>> FROM fedora:27
>>>> LABEL version="1.0"
>>>> LABEL author="@edt ASIX-M06"
>>>> LABEL subject="phpldapadmin"
>>>> RUN dnf -y install phpldapadmin php-xml httpd
>>>> RUN mkdir /opt/docker
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> WORKDIR /opt/docker
>>>> CMD /opt/docker/startup.sh
>>>> EXPOSE 80
>>>> ```

>>> #### 1.3.4 INICIALITZACIÓ CONTAINER

>>>> ##### 1.3.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:phpldapadmin
>>>>> ```

>>>> ##### 1.3.4.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:phpldapadmin
>>>>> ```
