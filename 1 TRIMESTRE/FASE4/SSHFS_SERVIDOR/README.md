Contenidor sshfs servidor

"Imatge basada en pam22:base"

Imatge sshfs que fa diferents accions depenet el argument pasat al entrypoint:
	
	-createHome:elimina si existeixen tots els homes i els crea de nou.
	Es tracta d’un script que ha de generar per a cada usuari LDAP el seu
	directori home als que s’accedeix via SSHFS.

	-syncHomes: sincronitza de nou els homes dels usuaris afegint aquells
	homes que no existeixen (usuaris nous al LDAP que no tenen encara
	un home creat al volum de homes).

	-start: Inicia el servei SSH permetent l’accés remot als homes dels
	usuaris (funcionament normal) suposant que el volum de dades ja
	conté els homes apropiats.


Configuracion startup.sh:


Mitjançant l'us de ldapsearch , grep i cut busquem els usuaris de ldap i els enmagatzem en la variable $usuaris_ldap 

"usuaris_ldap=$(ldapsearch -x -LLL uid | grep uid: | cut -d" " -f2)"

Amb l'us del case definim les opcions del entrypoint .
Configuracio createHomes:
case $1 in
	"createHomes")
		#Recorrem la llista d'usuaris ldap en un bucle for per a cada usuari
		for usuari in $usuaris_ldap
		do
		#Enmagatzem el home d'aquest usuari en la variable $home utilitzant getent, grep i cut
		home=$(getent passwd | grep "^$usuari:" | cut -d: -f6)
			#Posem un if per verificar la existencia del directori
			if  [ -d  $home ] 
			#Si existeix el borrem
			then
				rm -r $home
			#Si no el creem i li cambiem la propietat del directori al usuari ldap 
			else
				mkdir -p $home
				chown -R $usuari $home
			fi
		done
		#Per ultim encenem el servido ssh en foreground
		mkdir /run/sshd
		/usr/sbin/sshd  -D;;

Configuracio synHomes:

	"syncHomes")
		#Recorrem la llista d'usuaris ldap en un bucle for per a cada usuari
		for usuari in $usuaris_ldap
		do
		#Enmagatzem el home d'aquest usuari en la variable $home utilitzant getent, grep i cut
		home=$(getent passwd | grep "^$usuari:" | cut -d: -f6)
		#Posem un if per verificar que  directori no existeix
		if  ![ -d  $home ]
		#Si no existeix el creem li cambiem la propietat del directori al usuari ldap 
		then
			mkdir -p $home
			chown -R $usuari $home
		fi
		done
		#Per ultim encenem el servido ssh en foreground
		mkdir /run/sshd
		/usr/sbin/sshd  -D;;
Configuracio start:
	"start")
		# Encenem el servidor ssh en foreground amb les dades del volum
		mkdir /run/sshd
		/usr/sbin/sshd  -D;;
esac

configuracio Dockerfile:

Els paquets necesaris son:openssh-client openssh-server

la directiva entrypoint:
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
 



