> ### 2.1 SERVEI DNS

>> Implementar al cloud usant el mateix compose.yml un servidor DNS que permeti identificar tots els hosts de l’aula pel seu nom en el domini edt.org

>>> #### 2.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE2/DNS*

>>> #### 2.1.2 CREACIÓ D'ARXIUS

>>>>· __named.conf__

>>>>> ```
>>>>> options {	
>>>>>     listen-on port 53 { any; };
>>>>>     allow-query     { any; };
>>>>>
>>>>> logging {
>>>>>        channel default_debug {
>>>>>                file "data/named.run";
>>>>>                severity dynamic;
>>>>>        };
>>>>> };
>>>>>
>>>>> zone "edt.org" IN {
>>>>> type master;
>>>>> file "edt.org.zone";
>>>>> };
>>>>>
>>>>> zone "245.200.10.in-addr.arpa" IN {
>>>>> type master;
>>>>> file "reverse.edt.org.zone";
>>>>> };
>>>>>
>>>>> zone "." IN {
>>>>> 	      type hint;
>>>>>	      file "named.ca";
>>>>> };
>>>>>
>>>>> include "/etc/named.rfc1912.zones";
>>>>> include "/etc/named.root.key";
>>>>> ```

>>>>· __edt.org.zone__

>>>>> ```
>>>>> @ IN SOA dns admin.edt.org. ( 1 10800 3600 604800 38400 )
>>>>> @ IN NS dns
>>>>> dns A 172.18.0.2
>>>>> g01 A 10.200.245.201
>>>>> g02 A 10.200.245.202
>>>>> g03 A 10.200.245.203
>>>>> g04 A 10.200.245.204
>>>>> g05 A 10.200.245.205
>>>>> g06 A 10.200.245.206
>>>>> g07 A 10.200.245.207
>>>>> g08 A 10.200.245.208
>>>>> g09 A 10.200.245.209
>>>>> g10 A 10.200.245.210
>>>>> g11 A 10.200.245.211
>>>>> g12 A 10.200.245.212
>>>>> g13 A 10.200.245.213
>>>>> g14 A 10.200.245.214
>>>>> g15 A 10.200.245.215
>>>>> g16 A 10.200.245.216
>>>>> g17 A 10.200.245.217
>>>>> g18 A 10.200.245.218
>>>>> g19 A 10.200.245.219
>>>>> g20 A 10.200.245.220
>>>>> g21 A 10.200.245.221
>>>>> g22 A 10.200.245.222
>>>>> g23 A 10.200.245.223
>>>>> g24 A 10.200.245.224
>>>>> g25 A 10.200.245.225
>>>>> g26 A 10.200.245.226
>>>>> g27 A 10.200.245.227
>>>>> g28 A 10.200.245.228
>>>>> g29 A 10.200.245.229
>>>>> g30 A 10.200.245.230
>>>>> g31 A 10.200.245.231
>>>>> g32 A 10.200.245.232
>>>>> g33 A 10.200.245.233
>>>>> g34 A 10.200.245.234
>>>>> ```

>>>>· __reverse.edt.org.zone__

>>>>> ```
>>>>> @ IN SOA dns admin.edt.org. ( 1 10800 3600 604800 38400 )
>>>>> @ IN NS dns.edt.org.
>>>>> 2 IN PTR dns.edt.org.
>>>>> 201     PTR         g01.edt.org
>>>>> 202     PTR         g02.edt.org
>>>>> 203     PTR         g03.edt.org
>>>>> 204     PTR         g04.edt.org
>>>>> 205     PTR         g05.edt.org
>>>>> 206     PTR         g06.edt.org
>>>>> 207     PTR         g07.edt.org
>>>>> 208     PTR         g08.edt.org
>>>>> 209     PTR         g09.edt.org
>>>>> 210     PTR         g10.edt.org
>>>>> 211     PTR         g11.edt.org
>>>>> 212     PTR         g12.edt.org
>>>>> 213     PTR         g13.edt.org
>>>>> 214     PTR         g14.edt.org
>>>>> 215     PTR         g15.edt.org
>>>>> 216     PTR         g16.edt.org
>>>>> 217     PTR         g17.edt.org
>>>>> 218     PTR         g18.edt.org
>>>>> 219     PTR         g19.edt.org
>>>>> 220     PTR         g20.edt.org
>>>>> 221     PTR         g21.edt.org
>>>>> 222     PTR         g22.edt.org
>>>>> 223     PTR         g23.edt.org
>>>>> 224     PTR         g24.edt.org
>>>>> 225     PTR         g25.edt.org
>>>>> 226     PTR         g26.edt.org
>>>>> 227     PTR         g27.edt.org
>>>>> 228     PTR         g28.edt.org
>>>>> 229     PTR         g29.edt.org
>>>>> 230     PTR         g30.edt.org
>>>>> 231     PTR         g31.edt.org
>>>>> 232     PTR         g32.edt.org
>>>>> 233     PTR         g33.edt.org
>>>>> 234     PTR         g34.edt.org
>>>>> ```

>>> #### 2.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei PHPLDAPADMIN.

>>>> ```
>>>> cp /opt/docker/edt.org.zone /var/named/edt.org.zone
>>>> cp /opt/docker/named.conf /etc/named.conf
>>>> cp /opt/docker/reverse.edt.org.zone /var/named/reverse.edt.org.zone
>>>> chgrp named -R /var/named
>>>> chown -v root:named /etc/named.conf
>>>> restorecon -rv /var/named
>>>> restorecon /etc/named.conf
>>>> /usr/sbin/named -u named -f
>>>> ```

>>> #### 2.1.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> FROM centos:7
>>>> LABEL author="@gabrielggt66"
>>>> LABEL subject="dnsserver 2022"
>>>> RUN  yum install bind bind-utils nmap -y
>>>> RUN mkdir /opt/docker
>>>> WORKDIR /opt/docker
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> CMD /opt/docker/startup.sh
>>>> EXPOSE 53/udp 53/tcp
>>>> ```

>>> #### 2.1.4 INICIALITZACIÓ CONTAINER

>>>> ##### 2.1.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:dns
>>>>> ```

>>>> ##### 2.1.4.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm --name dns.edt.org -h dns.edt.org --net 2hisx -p 53:53/udp -p 53:53 --privileged -d marleneflor/projecte1t:dns
>>>>> ```
