cp /opt/docker/edt.org.zone /var/named/edt.org.zone
cp /opt/docker/named.conf /etc/named.conf
cp /opt/docker/reverse.edt.org.zone /var/named/reverse.edt.org.zone
chgrp named -R /var/named
chown -v root:named /etc/named.conf
restorecon -rv /var/named
restorecon /etc/named.conf
/usr/sbin/named -u named -f 


