> ### 2.2 SERVEI GRAFANA

>> Implementar en un container al clous usant el compose.yml un servei de monitorització Grafana. Aquest servei ha de poder ser accessible per els usuaris de LDAP i també per usuaris autenticats a través de proveïdors externs (Third party auth providers) com per exemple gmail.

>> El servei monitoritza el rendiment del sistema operatiu del host amfitrió del cloud on s’han desplegat els containers i també monitoritza cada un dels serveis individualment.

>>> #### 2.2.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE2/GRAFANA*

>>> #### 2.2.2 CREACIÓ DOCKERFILE

>>>> El contenidor grafana es munta amb una imatge Debian, on totes les ordres les executem amb l'usuari root.

>>>> També obrim els ports 3000,8086,8088
>>>> ```
>>>> FROM grafana/grafana
>>>> LABEL author="@projectit"
>>>> LABEL subject="grafanaserver 2022"
>>>> USER root
>>>> WORKDIR /opt/docker
>>>> COPY . /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> ENTRYPOINT ["/bin/bash","/opt/docker/startup.sh"]
>>>> ```

>>> #### 2.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>> És copien els fitxers de configuració de ldap als seus respectius llocs.

>>>> ```
>>>> #!/bin/bash
>>>> # Copiem els fitxers de configuració del servidor.
>>>> cp /opt/docker/grafana/grafana.ini /etc/grafana/grafana.ini
>>>> cp /opt/docker/grafana/ldap.toml /etc/grafana/ldap.toml
>>>>
>>>> #  dashboards i datasource per defecte.
>>>> mkdir /var/lib/grafana
>>>> cp -r /opt/docker/grafana/dashboards /var/lib/grafana/
>>>> cp -r /opt/docker/grafana/provisioning /usr/share/grafana/conf
>>>>
>>>> # Encenem serveis.
>>>> ./influxdb/usr/bin/influxd &
>>>> cd telegraf/usr/bin
>>>> ./telegraf --config telegraf.conf &
>>>> cd /usr/share/grafana/bin/
>>>> ./grafana-server --config /etc/grafana/grafana.ini
>>>> ```

>>> #### 2.2.4 CREACIÓ D'ARXIUS DE CONFIGURACIÓ

>>>>· __grafana/defaults.ini(grafana.ini)__

>>>>> *Dins del defaults.ini modifiquem el paràmetre __enabled__, que permet l'autenticació ldap ( per defecte false ).*

>>>>> ```
>>>>> [auth.ldap]
>>>>> enabled = true
>>>>> ```

>>>>· __grafana/ldap.toml__

>>>>> *Especifiquem el contenidor ldap i el port 389.*

>>>>> ```
>>>>> [[servers]]
>>>>> # Ldap server host (specify multiple hosts space separated)
>>>>> host = "ldap.edt.org"
>>>>> # Default port is 389 or 636 if use_ssl = true
>>>>> port = 389
>>>>> ```

>>>>> *Definim amb quin usuari de ldap farem les consultes en la base de dades. En aquest cas amb __Manager__ i el sue password per poder consultar a tos els usuaris.*

>>>>> ```
>>>>> # Search user bind dn
>>>>> bind_dn = "cn=Manager,dc=edt,dc=org"
>>>>> # Search user bind password
>>>>> # If the password contains # or ; you have to wrap it with triple quotes.
>>>>> bind_password = 'secret'
>>>>> ```

>>>>> *Posem com s'ha de buscar e identificar als usuaris de ldap. COm en l'última versió de ldap estan definits que s'identifiquin mitjançant __uid__ posem aquesta opció.*

>>>>> ```
>>>>> # User search filter, for example "(cn=%s)" or "(sAMAccountName=%s)" or "(uid=%s)"
>>>>> search_filter = "(uid=%s)"
>>>>> ```

>>>>> *Especifiquem la base de dades a la que és conectarà.*

>>>>> ```
>>>>> # An array of base dns to search through
>>>>> search_base_dns = ["dc=edt,dc=org"]
>>>>> ```

>>> #### 2.2.5 GOOGLE ( THIRD PARTY AUTH PROVIDERS )

>>>> Per habilitar __Google OAuth2__ en grafana, haurem d'enregistrar la nostra aplicació amb Google. Google generarà un __ID de client__ i __una clau secreta__ pel seu ús.

>>>>> ##### 2.2.5.1 CONFIGURACIÓ EN GOOGLE CLOUD

>>>>>> 1. Anem a <https://console.developers.google.com/apis/credentials>.

>>>>>> 2. Fem click a __Crear credenciales__, després fem click en __ID de client de OAuth__ en el menú desplegable.

>>>>>> 3. Introduïm el següent:

>>>>>>>· __Tipo de aplicación:__ *Aplicación web*

>>>>>>>· __Nombre:__ *Grafana*

>>>>>>>· __Orígenes autorizados de JavaScript:__ *<https://grafana.mycompany.com>*

>>>>>>>· __URL de redirección autorizadas:__ *<https://grafana.mycompany.com/login/google>*

>>>>>>>· *Reemplacem <https://grafana.mycompany.com> amb la URL de la nostra instancia de Grafana.*

>>>>>> 4. Fem click en __Crear__

>>>>>> 5. Copiem el ID del client i el secrel del cliente del model __Cliente OAuth__.

>>>>>> 6. Editem el fitxer __grafana.ini__.

>>>>>>>> ```
>>>>>>>> [auth.google]
>>>>>>>> enabled = true
>>>>>>>> allow_sign_up = true
>>>>>>>> client_id = 734414321237-nkdkp9i7b0id8rstmh180plsucue162t.apps.googleusercontent.com
>>>>>>>> client_secret = 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
>>>>>>>> scopes = https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/>>>>>>>> userinfo.email
>>>>>>>> auth_url = https://accounts.google.com/o/oauth2/auth
>>>>>>>> token_url = https://accounts.google.com/o/oauth2/token
>>>>>>>> api_url = https://www.googleapis.com/oauth2/v1/userinfo
>>>>>>>> allowed_domains =
>>>>>>>> hosted_domain =
>>>>>>>> ```

>>> #### 2.2.6 GITLAB ( THIRD PARTY AUTH PROVIDERS )

>>>> Els passos de configuració en __Gitlab__ són similars als de __Google__.

>>>>> ##### 2.2.6.1 CONFIGURACIÓ EN GITLAB

>>>>>> Editem el fitxer __grafana.ini__.

>>>>>>>> ```
>>>>>>>> #################################### GitLab Auth #########################
>>>>>>>> [auth.gitlab]
>>>>>>>> enabled = true
>>>>>>>> allow_sign_up = true
>>>>>>>> client_id = da2c6a8438e3d293be46959f070b1483923782efe6fde5eff32df8032f7e4094
>>>>>>>> client_secret = 185cdb4e846ab186f53f3cec15b7d6d79cb67508c6a9d1d8f124cf86f9e602cc
>>>>>>>> scopes = api
>>>>>>>> auth_url = https://gitlab.com/oauth/authorize
>>>>>>>> token_url = https://gitlab.com/oauth/token
>>>>>>>> api_url = https://gitlab.com/api/v4
>>>>>>>> allowed_domains =
>>>>>>>> allowed_groups =
>>>>>>>> role_attribute_path =
>>>>>>>> role_attribute_strict = false
>>>>>>>> allow_assign_grafana_admin = false
>>>>>>>> ```

>>> #### 2.2.7 CONFIGURACIÓ GRAFANA

>>>> __NOTA:__ Totes aquestes accions s'automatitzaran des del Dockerfile i startup.sh per incorporar-les al compose final.

>>>> ```
>>>> $ docker run --rm -d --name=grafana --user=root -p 3000:3000 -p 8086:8086 -p 8088:8088 --net=2hisx marleneflor/projecte1t:grafana
>>>> ```

>>>> Esquema lògic infraestructura: <https://www.jorgedelacruz.es/2020/11/23/en-busca-del-dashboard-perfecto-influxdb-telegraf-y-grafana-parte-i-instalando-influxdb-telegraf-y-grafana-sobre-ubuntu-20-04-lts/>

>>>> ![Infraestructura](https://www.jorgedelacruz.es/wp-content/uploads/2017/06/tig-monitor-logic.png)

>>>> ##### 2.2.7.1 INSTAL·LACIÓ I CONFIGURACIÓ DE INFLUXDB I TELEGRAF

>>>>> __Influxdb__

>>>>> ```
>>>>> $ wget https://dl.influxdata.com/influxdb/releases/influxdb-1.8.10_linux_amd64.tar.gz tar xvfz influxdb-1.8.10_linux_amd64.tar.gz
>>>>> ```

>>>>> __Telegraft__

>>>>> ```
>>>>> $ wget https://dl.influxdata.com/telegraf/releases/telegraf-1.25.0_linux_amd64.tar.gz tar xf telegraf-1.25.0_linux_amd64.tar.gz
>>>>> ```

>>>>> __Quedant-se les carpetes:__

>>>>>>· *influxdb-1.8.10-1*

>>>>>>· *telegraf-1.25.0*

>>>>> Per comoditat les anomenem com __influxdb__ i __telegraf__.

>>>>> Fitxer de config telegraf amb plugins necessaris per a la monitorització del sistema i del servei __opendldab__.

>>>>> Creem un fitxer de configuració de config base.

>>>>> ```
>>>>> cd telegraf
>>>>> ./telegraf -sample-config -input-filter >>>>> cpu:kernel:mem:swap:disk:diskio:system:net:processes:openldap -output-filter influxdb > >>>>> telegraf.conf
>>>>> ```

>>>>> Editem el fitxer de cofig de __telegraf.conf__ per recopilar mètriques del servidor OpenLDAP especificant l'adreça IP o el nom de host i el número de port correcte .

>>>>> ```
>>>>> vi telegraf.conf
>>>>> # OpenLDAP cn=Monitor >>>>> plugin                                                                             
>>>>> [[inputs.>>>>>openldap]]                                                                               
>>>>> host = "ldap.edt.org"         # cambiar pro hotsname del container de ldap                                                              
>>>>>  port = 389     
>>>>>
>>>>>  # dn/password to bind with. If bind_dn is empty, an anonymous bind is performed. 
>>>>>  bind_dn = "cn=Manager,dc=edt,dc=org"                                                                   
>>>>>  bind_password = "secret"
>>>>> ```

>>>>> Llancem els executables __influxdb__ i __telegraf__ en __background.__

>>>>> ```
>>>>> workdir /opt/docker
>>>>> ./influxdb/usr/bin/influxd & 
>>>>> ./telegraf --config telegraf.conf & 
>>>>> ```

>>>>> Validacions en la BBDD de influxdb (opcional) desde el dir __influxdb/usr/bin/__.

>>>>> ```
>>>>> ./influx 
>>>>> Connected to http://localhost:8086 version 1.8.10
>>>>> InfluxDB shell version: 1.8.10
>>>>> > 
>>>>> show databases
>>>>> use telegraf
>>>>> show MEASUREMENTS
>>>>> ```

>>> #### 2.2.8 CONNEXIÓ GRAFANA

>>>> Connectar-se per la web: <http://localhost:3000/>

>>>>>· user admin
>>>>>· passwd admin

>>>> ##### 2.2.8.1 CONFIGURATION Y DASHBOARDS

>>>>> Definim la nostra __Data Source (InfluxDB)__

>>>>> ![Data Source](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/Source.JPG)

>>>>> __URL:__ *<http://localhost:8086> , la ip del host i sent el port 8086 per ón escolta influxdb.*

>>>>> ![Influxdb](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/influxdb.JPG)

>>>>> La BBDD és __telegraf__, creada per defecte sense user ni password. 

>>>>> Per la creació dels __dashboard__ importarem els fitxers __.json__ disponibles en la web de grafana.

>>>>> ![Dashboard](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/dashboard.JPG)

>>>>> ![Dashboard2](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/dashboard2.JPG)

>>>>> Finalment visualitzem les métricas obtingudas del sistema del host i del servi __openldap__.

>>>>> ![Métricas](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/metricas.JPG)