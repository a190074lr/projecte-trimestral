> PAM
```
 docker run --rm pam.edt.org -h pam.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:pam
```
> LDAP
```
 docker run --rm ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:ldap
```
> PHPLDAPADMIN
```
docker run --rm phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:phpldapadmin
```
> DNS
```
 docker run --rm --name dns.edt.org -h dns.edt.org --net 2hisx -p 53:53/udp -p 53:53 --privileged -d marleneflor/projecte1t:dns
```
> GRAFANA
```
docker run --rm -d --name=grafana --user=root -p 3000:3000 -p 8086:8086 -p 8088:8088 --net=2hisx marleneflor/projecte1t:grafana
```
> SSHFS
```
docker run --rm --name sshfs.edt.org -h sshfs.edt.org --net 2hisx --privileged -v ldap-home:/tmp -d marleneflor/projecte1t:sshfs createHomes
```

```
docker run --rm --name sshfs.edt.org -h sshfs.edt.org --net 2hisx --privileged -v ldap-home:/tmp -d marleneflor/projecte1t:sshfs start
```
```
docker run --rm --name sshfs.edt.org -h sshfs.edt.org --net 2hisx --privileged -v ldap-home:/tmp -d marleneflor/projecte1t:sshfs start
```
