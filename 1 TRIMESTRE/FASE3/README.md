## 3. FASE 3: MUNTATGE DE HOMES DELS USUARIS AMB NFS

> En aquesta fase cal que els usuaris LDAP disposin d’un home d’usuari muntat des
d’un servidor NFS que contindrà en un volum els homes dels usuaris

> ### 3.1 SERVEI NFS

>> Desplegar al cloud usant el mateix fitxer compose.yml un servidor NFS que conté en
un volum els directoris home dels usuaris LDAP. Aquest container ha de permetre
autenticació LDAP. S’ha de poder configurar segons el valor d’argument passat a
l’entry point:

>>>· __createHomes:__ *Elimina si existeixen tots els homes i els crea de nou. Es tracta d’un script que ha de generar per a cada usuari LDAP el seu directori home en un recurs compartit el NFS.*

>>>· __syncHomes:__ *Sincronitza de nou els homes dels usuaris afegint aquells homes que no existeixen (usuaris nous al LDAP que no tenen encara un home creat al volum de homes).*

>>>· __start:__ *Inicia el servei NFS exportant els homes dels usuaris (funcionament normal) suposant que el volum de dades ja conté els homes apropiats.*

>>> #### 3.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE3*

>>> #### 3.1.2 ARXIUS

>>>> *Haurem de disposar dels arxius __exports__, __lapd.conf__, __nslcd.conf__, __nsswitch.conf__, __pam_mount.conf__, configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>> #### 3.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>> ```
>>>> #! /bin/bash
>>>>
>>>> # crear usuaris unix
>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>> do
>>>>   useradd -m -s /bin/bash $user
>>>>   echo -e "$user\n$user" | passwd $user
>>>> done
>>>>
>>>> # engegar serveis per fer el pam_ldap
>>>> /usr/sbin/nslcd
>>>> /usr/sbin/nscd
>>>>
>>>> # deixar el container interactiu
>>>> /bin/bash
>>>>
>>>> ```

>>> #### 3.1.3 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # PAM Server 2022>>>>
>>>> FROM debian
>>>> LABEL author="@projectit"
>>>> LABEL subject="NFS 2022"
>>>> # variable para hacer la instalacion desatendida, no interactiva
>>>> ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get update && apt-get -y install procps tree nmap vim iproute2 libpam-mount libnss-ldapd libpam-ldapd nfs-kernel-server
>>>> RUN mkdir /opt/docker
>>>> WORKDIR /opt/docker
>>>> COPY * /opt/docker/
>>>> COPY ldap.conf /etc/ldap/
>>>> COPY nslcd.conf /etc/
>>>> COPY common-session /etc/pam.d/
>>>> COPY pam_mount.conf.xml /etc/security/
>>>> COPY nsswitch.conf /etc/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> CMD /opt/docker/startup.sh
>>>> ```

>>> #### 3.1.4 INICIALITZACIÓ CONTAINER

>>>> ##### 3.1.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:nfs
>>>>> ```