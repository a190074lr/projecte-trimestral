# Instalacio Servidor Freeipa

## Vagrant
Tota la distribucio per a la gestio de les maquines ,tant servidors com hosts , es fan mitjançant Vagrant.

Aquest es llança amb la creacio de un Vagrantfile com aquest utilitzat per al Freeipa:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

# Maquina Freipa servidor
Vagrant.configure("2") do |config|
  config.vm.define "freeipa" do |server|
    server.vm.box = "generic/fedora35"
    server.vm.provider "virtualbox" do |vb|
      vb.memory = "2000"
      vb.cpus = "2"
    end
    server.vm.hostname = "smb.ipa.edt.edu"
    server.vm.network "private_network", ip: "192.168.56.2"
    server.vm.provision "file", source: "./files/sysctl.conf", destination: "/$HOME/"
    server.vm.provision "file", source: "./files/hosts", destination: "/$HOME/"
    server.vm.provision "file", source: "./files/config", destination: "/$HOME/"
    server.vm.provision "file", source: "./files/ipa-options-ext.conf", destination: "/$HOME/"
    server.vm.provision "file", source: "./files/smb.conf", destination: "/$HOME/"
    server.vm.provision "file", source: "./users.sh", destination: "/$HOME/"
    server.vm.provision "file", source: "./recursos.sh", destination: "/$HOME/"
    server.vm.provision "shell", path: "./startup.sh"
  end
end

```

## LLençament
Despres d'obtenir altres fitxers de configuracio necesaris juntament amb el Vagrantfile llancem el servidor amb l'ordre:

```
vagrant up
```
Des d'quest moment un munt de procesos s'iniciaran mostrarnt cadascun el seu output:

## Configuracio basica
En la seguent imatge s'aprecia l'interfaz Freeipa i com s'esculliran uns valors per al domini,hostname i reialme.

![imagen](imagenes/ipa1.png)

## Configuracio DNS
Seguidament ve la configuracio del DNS respecte a altres servidors DNS.
![imagen](imagenes/ipd_dns_forward_ad.png)
## Comprobacio DNS
Dins de la configuracio hi ha un apartat per comprovar el DNS
![imagen](imagenes/ipa3_dns_checks.png)
## Creacio grups
Fem un llistat dels grups

![imagen](imagenes/create_grups.png)
## Creacio usuaris
Fem un llistat dels usuaris on veiem les dades desl usuaris creats.

![imagen](imagenes/create_users.png)

# Instalacio client Freeipa (Centos)
Per ultim instalem un client Freeipa que es sinronitzara automaticament amb els servidor

![imagen](imagenes/centos_clie.png)


Tambe es pot observar la configuracio client del samba
![imagen](imagenes/centos_clie_2.png)