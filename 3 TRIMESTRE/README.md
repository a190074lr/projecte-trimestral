>>>>>>>>>>>>>> # PROJECTE FINAL

## INTEGRANTS:

> *· Maria Marlene Flor Benitez.*
>
> *· Kevin Gabriel Gonzalez Trujillo.*
>
> *· Bruno Rodríguez Aranibar.*
>
> *· Lucas Rodríguez Cabañeros.*

## ENLLAÇOS

· __GITLAB__

> <https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/3%20TRIMESTREE>

· __DOCKER HUB__

> <https://hub.docker.com/u/proyecto2hisx>

## REQUERIMENTS GENERALS

> *· Cal fer grups de dos alumnes per fer aquesta pràctica.*

> *· Cal disposar de 4 ordinadors a l’aula, 2 que són les estacions de treball habituals dels alumnes i dos mini-debian.*

> *· La pràctica es desenvolupa com un projecte al gitlab entre els dos alumnes.*

> *· Cal fer tota la documentació en markdown.*

> *· Generar una presentació fent-la en markdown i convertir-la amb pandoc a presentació*

> *· Fer una exposició del projecte amb un temps màxim de 20 minuts.*

> *· Elaborar un poster del projecte amb el format que es proporcionarà i que ha d’incloure un codi QR que enllaci al Git del projecte.*

## INTRODUCCIÓ DEL PROJECTE

> El projecte final a desenvolupar aquest tercer trimestre és un projecte global que barca tots
els coneixements adquirits al cicle i que representa un repte d’implementació d’un projecte
real d’administració de sistemes informàtics. Aquest repte consisteix en implementar el
desplegament i administració del sistema informàtic d’una escola com per exemple l’Escola
del Treball de Barcelona, en el nostre cas la coneguda __*edt.org.*__

> Es tracta de dissenyar i implementar a petita escala el desplegament que es produeix a
l’escola, on fa falta una administració centralitzada i la gestió d’equips i sistemes operatius
heterogenis.

> El nucli principal del projecte és la implementació d’un servei centralitzat de gestió tant
d’usuaris com de serveis i màquines, aplicant una eina d’administració d’identitats com
__*FreeIPA*__. Aquesta eina incorpora per sota serveis elementals i ja coneguts com LDAP,
Kerberos, NFS i SAMBA. Així doncs, la gestió centralitzada d’usuaris ha de permetre
disposar dels directoris homes en qualsevol de les màquines de l’organització.

> L’escola incorpora sistemes operatius heterogenis que caldrà combinar, hi haurà equips
GNU/Linux de diferents distribucions però també hi ha equips Windows amb versions de
Windows diferents. Tots junts han de funcionar en una sola organització.

> Una altra eina àmpliament utilitzada en la integració de sistemes heterogenis és
__*SAMBA-4-AD*__, amb SAMBA actuant com a Active Directory, administrador d’un domini
Windows. Caldrà explorar la seva implementació la interrelació amb FreeIPA.

> L’existència de dominis diferents permet introduir el concepte de federació d’identitats,
confiança entre dominis i integració de dominis. Tots aquests conceptes han de ser abordats
en l’elaboració del projecte.

> Altres reptes tecnològics que es poden incloure opcionalment són avançar en aspectes i
detalls concrets de les tecnologies i serveis implicats, per exemple la delegació de zones
DNS, la configuració de subdominis LDAP, servidors principals i secundaris, servei DHCP
per a xarxes VLAN diferents, etc.

> A grans trets el nucli d’aquest projecte és la implementació de sistemes de gestió
d’identitats __*Identity Management*__ i d’__*integració de sistemes heterogenis*__, continguts que
corresponen precisament a una de les certificacions GNU/Linux LPI de nivell avançat
[LPIC-3 Mixed Environments](https://www.lpi.org/our-certifications/lpic-3-300-overview).

## 1. FASE 1: IMPLEMENTACIÓ DEL SERVEI FREEIPA

> ### 1.1 QUE ÉS UN GESTIÓ D'IDENTIDAD?

> La gestió d'identitat (Identity Management) es refereix al conjunt de processos, polítiques i tecnologies utilitzades per a administrar i controlar les identitats digitals dels usuaris en un entorn de TU. Implica la gestió d'informació i privilegis relacionats amb la identitat dels usuaris, com a noms d'usuari, contrasenyes, rols, permisos i atributs addicionals.

> L'objectiu principal de la gestió d'identitat és garantir que només les persones autoritzades tinguin accés als recursos i serveis adequats en un sistema o xarxa. En implementar la gestió d'identitat, les organitzacions poden establir controls de seguretat, complir amb requisits de compliment normatiu i simplificar l'administració d'usuaris.

> Les funcions clau de la gestió d'identitat inclouen són l'Autenticació, Autorització, Provisionamiento, Gestió de contrasenyes i Federació d'identitat.


> ### 1.2 QUE ÉS FREEIPA?

>> FreeIPA és una eina d'administració d'identitat i accés de codi obert que s'utilitza principalment en entorns de servidors Linux. Proporciona un conjunt complet de serveis com LDAP, Kerberos,NTP, DNS, Dogtag ,etc per a gestionar l'autenticació, autorització i política de seguretat en un domini. El seu nom indica *__Identitat, Polítiques, Autenticació Free__*
(Lliure). La administració dels serveis que dona FreeIPA es pot realitzar a través de línia de comandes com també mitjançant una interfície web.

>>> ![esquema_freeipa](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/3%20TRIMESTRE/images/Esquema_FreeIPA.png)

>> __· LDAP:__ *FreeIPA utilitza el protocol LDAP (Lightweight Directory Access Protocol) per a emmagatzemar i administrar informació d'identitat i accés, com a usuaris, grups, polítiques i altres atributs. LDAP és un estàndard de la indústria per a la gestió de directoris i permet una organització jeràrquica i eficient de la informació.*

>> __· Kerberos KDC:__ *FreeIPA utilitza aquest protocol d’ autenticació simètric per a permetre als usuaris autenticar-se de manera segura en el domini. Kerberos emet tiquets d'autenticació que s'utilitzen per a accedir als serveis i recursos protegits dins de l'entorn.*

>> __· NTP:__ *FreeIPA utilitza el protocol de sincronització de temps per la xarxa NTP per
poder sincronitzar el rellotge dels sistemes. Aquesta sincronització es essencial
per alguns components i serveis com pot ser el d’ autenticació i certificació.
FreeIPA té la possibilitat de ser el servidor NTP i que faci de servidor NTP a altres
computadors de la xarxa.*

>> __· DNS:__ *Inclou un servidor DNS integrat que proporciona serveis de resolució de noms per a les màquines i serveis dins del domini. Proporciona una gestió centralitzada de les zones DNS i permet assignar noms de domini als recursos, serveis dins del domini de FreeIPA com també permet la utilització d’ altres servidors DNS externs.*

>> __· Entitat Certificadora CA:__ *FreeIPA inclou una Autoritat de Certificació interna que emet i signatura els certificats per a usuaris i serveis dins de l'entorn FreeIPA. La CA de FreeIPA és responsable de la generació de claus, la signatura de certificats i la gestió de la cadena de confiança. Alguns dels serveis que necessita de la CA pot ser Kerberos. FreeIPA dona servei de CA a través del
projecte Dogtag Certificate Services. Dogtag integra una infraestructura de
clau pública PKI la qual permet signar, publicar o revocar certificats.*

>> __· Samba4:__ *FreeIPA pot integrar-se amb Samba per a permetre l'autenticació i l'accés a recursos compartits d'arxivaments en entorns mixtos de Linux i Windows. Això permet als usuaris accedir a recursos compartits d'arxivaments utilitzant les seves credencials de FreeIPA.*

>>> ![esquema_freeipa2](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/3%20TRIMESTRE/images/Esquema_FreeIPA2.png)

>> Un administrador de FreeIPA utilitza la interfície d'administració per a agregar una nova màquina.
L'administrador proporciona detalls sobre la màquina, com a nom de host, adreça IP, sistema operatiu, grup al qual pertany, etc. La informació de la màquina s'emmagatzema en el directori LDAP de FreeIPA, la qual cosa permet la gestió centralitzada i el seguiment de les màquines dins de l'entorn.

>> __System Security Services Daemon o SSSD__ *Permet l'autenticació i autorització basades en FreeIPA en sistemes Linux i garanteix una experiència d'inici de sessió coherent per als usuaris en diferents sistemes. Permet la resolució de noms d'usuaris i grups en identificadors únics (UID i GID) utilitzant els serveis de directori de FreeIPA millorant el rendiment mitjançant l'ús d'una caixet local i facilitar la integració amb altres components del sistema.*

__Gestió de renovació de certificats amb Certmonger__ *Certmonger és un dimoni utilitzat per FreeIPA per a la gestió de certificats. Automatitza les tasques de renovació i sol·licitud de certificats per a garantir que els certificats utilitzats a l'entorn de FreeIPA estiguin sempre actualitzats i vàlids.*

> ### 1.3 INSTAL·LACIÓ SERVIDOR FREEIPA

>> La instalacio del servidor Freeipa s'efectuara amb l'eina de gestio de maquines virtuals Vagrant. 

>> El fitxer necesari amb les especificacions per efectuar el llançament del servei conte el seguent contingut:

>> ### 1.3.1 VAGRANTFILE

>>> ```
>>> # -*- mode: ruby -*-
>>> # vi: set ft=ruby :
>>>
>>> # Maquina Freipa servidor
>>> Vagrant.configure("2") do |config|
>>>  config.vm.define "freeipa" do |server|
>>>    server.vm.box = "generic/fedora35"
>>>    server.vm.provider "virtualbox" do |vb|
>>>      vb.memory = "2000"
>>>      vb.cpus = "2"
>>>    end
>>>    server.vm.hostname = "smb.ipa.edt.edu"
>>>    server.vm.network "private_network", ip: "192.168.56.2"
>>>    server.vm.provision "file", source: "./files/sysctl.conf", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./files/hosts", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./files/config", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./files/ipa-options-ext.conf", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./files/smb.conf", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./users.sh", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./recursos.sh", destination: "/$HOME/"
>>>    server.vm.provision "shell", path: "./startup.sh"
>>>  end
>>> end
>>>```

>>> Per llençar el Vagrant hi ha que executar la comanda seguent en el directori amb el fitxer Vagrantfile

>>>```
>>> vagrant up
>>>```

>>> ### 1.3.1.1 INSTAL·LACIÓ INCIAL

>>>> En aquest apartat especifiquem els requeriments bascis que tindra la nostra maquina virtual.

>>>>  · __Nom secció de configuració:__ *Server.*

>>>>  · __Imatge utilitzada:__ *S'utilitzara la imatge **generic/fedora35**.*

>>>>  · __Nom del proveidor:__ *Virtualbox.*

>>>>  · __Memoria:__ *2000 Mib.*

>>>>  · __Numero de CPUs:__ *2.*

>>>>  · __Nom de la maquina:__ *smb.ipa.edt.edu.*

>>>>  · __IP dins la xarxa:__ *Tindra la IP 192.168.56.2*

>>>>> ```
>>>>> Vagrant.configure("2") do |config|
>>>>>  config.vm.define "freeipa" do |server|
>>>>>    server.vm.box = "generic/fedora35"
>>>>>    server.vm.provider "virtualbox" do |vb|
>>>>>      vb.memory = "2000"
>>>>>      vb.cpus = "2"
>>>>>    end
>>>>>    server.vm.hostname = "smb.ipa.edt.edu"
>>>>>    server.vm.network "private_network", ip: "192.168.56.2"
>>>>> ```

>>> ### 1.3.1.2 APROVISIONAMENT DE FITXERS

>>>> En aquesta seccio traslladem els fitxers necesaris per a la configuracio del servidor dins de  la maquina virtual.

>>>>> ```    
>>>>>    server.vm.provision "file", source: "./files/sysctl.conf", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./files/hosts", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./files/config", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./files/ipa-options-ext.conf", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./files/smb.conf", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./users.sh", destination: "/$HOME/"
>>>>>    server.vm.provision "file", source: "./recursos.sh", destination: "/$HOME/"
>>>>> ```

>>>>  · __sysctl.conf:__ *Configuració de Ipv6.*

>>>>  · __hosts:__ *Fitxer amb noms de host i la seva IP.*

>>>>  · __config:__ *Fitxer de configuracio de seguretat SELinux.*

>>>>  · __ipa-options-ext.conf:__ *Fitxer de configuracio adicional bind (DNS).*

>>>>  · __smb.conf:__ *Fitxer de recursos compartits pel servei Samba.*

>>>>  · __users.sh:__ *Script bash de creacio de usuaris i grups Freeipa.*

>>>>  · __recursos.sh:__ *Script bash de creacio de directoris i fitxers utilitzats posteriorment per Samba*

>>> ### 1.3.1.3 CONFIGURACIÓ SERVIDOR

>>>> S'executa desde el directori local el script startup.sh on es troben totes les comandes necesaries per configurar Freeipa.


>>>>> ```
>>>>>    server.vm.provision "shell", path: "./startup.sh"
>>>>>  end
>>>>> end
>>>>> ```

>>>> · __startup.sh:__ *Script de bash amb les ordres necesaries per llançar Freeipa.*

>> ### 1.3.2 FITXERS DE CONFIGURACIÓ

>>> Per a poder aprovionar a Freipa amb la configuracio necesaria es necesiten el contingut de diferents recursos que involucren distintes funcionalitats i serveis.

>>> __SYSTCTL.CONF__

>>>> *Aquest fitxer conte la configuracio actual de la maquina virtual respecte  a la configuracio del IPv6.*

>>>> ```
>>>> # sysctl settings are defined through files in
>>>> # /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
>>>> #
>>>> # Vendors settings live in /usr/lib/sysctl.d/.
>>>> # To override a whole file, create a new file with the same in
>>>> # /etc/sysctl.d/ and put new settings there. To override
>>>> # only specific settings, add a file with a lexically later
>>>> # name in /etc/sysctl.d/ and put new settings there.
>>>> #
>>>> # For more information, see sysctl.conf(5) and sysctl.d(5).
>>>>
>>>> net.ipv6.conf.all.disable_ipv6 = 0
>>>> ```

>>>>  ·__net.ipv6.conf.all.disable_ipv6 = 0__ *Activa la funcionalitat IPv6 posant el valor 0.*

>>> __HOSTS__

>>>> *Fitxers que conte la resolucio de noms i associacio de nom amb IP*

>>>> ```
>>>> # Loopback entries; do not change.
>>>> # For historical reasons, localhost precedes localhost.localdomain:
>>>> 127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
>>>> ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
>>>> # See hosts(5) for proper format and other examples:
>>>> # 192.168.1.10 foo.mydomain.org foo
>>>> # 192.168.1.13 bar.mydomain.org bar
>>>>
>>>> 127.0.0.1    fedora37 fedora37.localdomain
>>>> 192.168.56.2 smb.ipa.edt.edu smb
>>>> 192.168.57.2 windows.ad.edt.com windows
>>>> ```

>>>> · __127.0.0.1 fedora37 fedora37.localdomain:__ *Resolució de nom per al loopback.*

>>>> · __192.168.56.2 smb.ipa.edt.edu smb:__ *Resolució de nom per al servidor Freeipa amb .*hostname **smb.ipa.edt.edu** o **smb** a la IP **192.168.56.2**

>>>> · __192.168.57.2:__ *Resolució de nom per al Windows server amb hostname **windows.ad.edt.com** o  **windows** per a la IP **192.168.57.2**.*

>>> __CONFIG__

>>>> *Fitxer de configuracio de seguretat SELinux.*

>>>> ```
>>>> # This file controls the state of SELinux on the system.
>>>> # SELINUX= can take one of these three values:
>>>> #     enforcing - SELinux security policy is enforced.
>>>> #     permissive - SELinux prints warnings instead of enforcing.
>>>> #     disabled - No SELinux policy is loaded.
>>>> # See also:
>>>> # https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-selinux/#getting-started-with-selinux-selinux-states-and-modes
>>>> #
>>>> # NOTE: In earlier Fedora kernel builds, SELINUX=disabled would also
>>>> # fully disable SELinux during boot. If you need a system with SELinux
>>>> # fully disabled instead of SELinux running with no policy loaded, you
>>>> # need to pass selinux=0 to the kernel command line. You can use grubby
>>>> # to persistently set the bootloader to boot with selinux=0:
>>>> #
>>>> #    grubby --update-kernel ALL --args selinux=0
>>>> #
>>>> # To revert back to SELinux enabled:
>>>> #
>>>> #    grubby --update-kernel ALL --remove-args selinux
>>>> #
>>>> SELINUX=disabled
>>>> # SELINUXTYPE= can take one of these three values:
>>>> #     targeted - Targeted processes are protected,
>>>> #     minimum - Modification of targeted policy. Only selected processes are protected.
>>>> #     mls - Multi Level Security protection.
>>>> SELINUXTYPE=targeted
>>>> ```

>>>> ·__SELINUX=disabled:__ *Deshabilita la seguretat SELinux el cual permet instalar els paquest necesaris per el servidor.*

>>> __IPA-OPTIONS-EXT.CONF__

>>>> *Fitxer de configuracio adicional per al servei BIND named.*

>>>> ```
>>>> /* User customization for BIND named
>>>> *
>>>> * This file is included in /etc/named.conf and is not modified during IPA
>>>> * upgrades.
>>>> *
>>>> * It must only contain "options" settings. Any other setting must be
>>>> * configured in /etc/named/ipa-ext.conf.
>>>> *
>>>> * Examples:
>>>> * allow-recursion { trusted_network; };
>>>> * allow-query-cache { trusted_network; };
>>>> */
>>>>
>>>> /* turns on IPv6 for port 53, IPv4 is on by default for all ifaces */
>>>> listen-on-v6 { any; };
>>>>
>>>> /* dnssec-enable is obsolete and 'yes' by default */
>>>> dnssec-validation no;
>>>> ```

>>>> ·__listen-on-v6 { any; };:__ *Habilita la funcionalitat IPv6 per al servidor DNS per a cualsevol port.*

>>>> ·__dnssec-validation no;:__ *Deshabilita la validacio per dnssec.*

>> ### 1.3.3 SCRIPTS BASH

>>> __USERS.SH__

>>>> *Script de bash de creacio de usuaris i grups Freeipa mitjançant ordres ipa.*

>>>> ```
>>>>#! /bin/bash
>>>>
>>>># list users 
>>>>users="pere marta anna alice bob mallory" 
>>>>
>>>>#email domain 
>>>>domain=$(domainname)
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>># create users 
>>>>echo "*****CREATING USERS******"
>>>> 
>>>>for user in $users
 >>>>do 
   >>>>echo -e "k$user\nk$user" | ipa user-add $user --first=$user --last=$user --shel=/bin/bash --email=$user@$domain --password
>>>> done
>>>>
>>>>############GRUPS###################
>>>>#list grups 
>>>>grups="alumnes profesors" #admins
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>>for grupname in $grups
>>>>do
>>>> ipa group-add $grupname
>>>>done
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>>#afegir usuaris als grups
>>>>ipa group-add-member alumnes --users={marta,pere,anna}
>>>>ipa group-add-member profesors --users={bob,alice}
>>>>ipa group-add-member admins --users=mallory
>>>>
>>>>exit 0
>>>> ```

>>> __CREACIÓ USUARIS__

>>>> ```
>>>> ############ USERS ###################
>>>>#! /bin/bash
>>>>
>>>># list users 
>>>>users="pere marta anna alice bob mallory" 
>>>>
>>>>#email domain 
>>>>domain=$(domainname)
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>># create users 
>>>>echo "*****CREATING USERS******"
>>>> 
>>>>for user in $users
 >>>>do 
   >>>>echo -e "k$user\nk$user" | ipa user-add $user --first=$user --last=$user --shel=/bin/bash --email=$user@$domain --password
>>>> done
>>>>
>>>> ```

>>>>  · __users:__ *Variable que conte una llista amb els noms dels usuaris que crearem.*

>>>>  . __domain:__ *Variable que conte el nom del domini*

>>>>  . __kinit admin && echo "OK INIT":__ *Ordre de kerberos que crea un ticket amb l'usuari admin proveint la contraseña de forma interactiva amb echo -e __"k$user\nk$user"__*

>>>>  · __user:__ *Variable amb el nom de un usuari .*

>>>>  · __ipa user-add:__ *Ordre de Freeipa per a crear usuaris.*

>>>>>    - __--first=$user:__ *Primer nom del usuari.*

>>>>>    - __--last=$user:__ *Ordre de Freeipa per a crear usuaris.*

>>>>>    - __--shel=/bin/bash:__ *Especifica el shell del usuari.*

>>>>>    - __--email=$user@$domain:__ *Crea un email per al usuari.*

>>>>>    - __--password:__ *Especifica el password de l'usuari i proporciona de forma automatitzada el password amb ___echo -e "k$user\nk$user".*

>>>>>    - __for user in $users:__ *Amb aquest bucle repetim el proces per a crear els usuaris*

>>> __CREACIÓ GRUPS__

>>>> ```
>>>>############GRUPS###################
>>>>#list grups 
>>>>grups="alumnes profesors" #admins
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>>for grupname in $grups
>>>>do
>>>> ipa group-add $grupname
>>>>done
>>>>
>>>>echo -e "jupiter123\n" | kinit admin && echo "OK INIT"
>>>>
>>>># afegir usuaris als grups
>>>>ipa group-add-member alumnes --users={marta,pere,anna}
>>>>ipa group-add-member profesors --users={bob,alice}
>>>>ipa group-add-member admins --users=mallory
>>>>
>>>>exit 0
>>>>
>>> ```

>>>>  · __grups:__ *Variable que conte una llista dels grups que es crearan.Menys admins que ya existeix per defecte*

>>>>  . __kinit admin && echo "OK INIT":__ *Ordre de kerberos que crea un ticket amb l'usuari admin proveint la contraseña de forma interactiva amb echo -e __"k$user\nk$user"__*



>>>>  · __grupname:__ *Variable que conte un grup de la llista __grups__.*

>>>>  · __ipa group-add $grupname::__ *Ordre de Freeipa que fa la creacio dels grups.*

>>>>>    - __for grupname in $grups:__ *Bucle for que repeteix l'ordre de creacio de grups Freeipa hasta crear tots els grups de __grups__.*

>>>>  · __ipa group-add-member alumnes --users={pere,marta,anna}:__ *Ordre Freeipa que afegeix usuaris al grup alumnes.*

>>>>  · __ipa group-add-member profesors --users={bob,alice}:__ *Ordre Freeipa que afegeix usuaris al grup professors.*

>>>>  · __ipa group-add-member admins --users=mallory:__ *Ordre Freeipa que afegeix usuaris al grup admins.*

>>> __RECURSOS.SH__

>>>> *Script bash de creacio de directoris i fitxers utilitzats posteriorment per Samba.*

>>>> ```
>>>> #! /bin/bash 
>>>> mkdir  /mnt/notes /mnt/examenes /mnt/fitxers
>>>> touch /mnt/examenes/m0{6..8}
>>>> touch /mnt/fitxers/bbdd /mnt/fitxers/config /mnt/fitxers/Dockerfile
>>>> touch /mnt/notes/primertrimestre /mnt/notes/segundotrimestre /mnt/notes/tercertrimestre
>>>>
>>>> cp /home/vagrant/smb.conf /etc/samba/smb.conf
>>>> ipactl restart
>>>>
>>>> echo "RECURSOS COMPARTIDOS OK"
>>>> ```

>>> __STARTUP.SH__

>>>> *Script de bash principal que fa tota la feina de configuracio aprovisionament i llançament del servidor ipa i de serveis secundaris.*

>>>> ```
>>>> #! /bin/bash
>>>>
>>>> dnf -y update
>>>> dnf -y install nmap tree net-tools freeipa-* bind bind-chroot
>>>> 
>>>> cp /home/vagrant/config /etc/selinux/config
>>>> cp /home/vagrant/sysctl.conf /etc/sysctl.conf
>>>> cp /home/vagrant/hosts /etc/hosts
>>>> sysctl -p
>>>>
>>>> # Activate FreeIpa server
>>>> ipa-server-install -a jupiter123 -p jupiter123 --domain=ipa.edt.edu --realm=IPA.EDT.EDU --hostname smb.ipa.edt.edu --ip-address=192.168.56.2 --mkhomedir --setup-dns >>>> --ssh-trust-dns --forwarder=8.8.8.8 --auto-reverse --no-host-dns -U
>>>>
>>>> # Config firewall
>>>> firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
>>>> firewall-cmd --add-port={138,1024-1300,3268}/tcp --permanent
>>>> firewall-cmd --add-port={123,138,53,139,445}/udp --permanent
>>>> firewall-cmd --reload
>>>>
>>>> # Check dns service 
>>>> echo -e "jupiter123\n" | kinit admin && echo "CHECK DNS OK" 
>>>> dig smb.ipa.edt.edu
>>>> ipa host-show smb.ipa.edt.edu
>>>>
>>>> cp /home/vagrant/ipa-options-ext.conf /etc/named/ipa-options-ext.conf && echo "COPY OK" 
>>>> ipactl restart
>>>>
>>>> # Trust AD
>>>> yum install -y samba-*
>>>> echo -e "jupiter123\n" | kinit && echo "OK TICKET AD"
>>>> klist
>>>> ipa dnsforwardzone-add ad.edt.com --forwarder=192.168.57.2 --forward-policy=only
>>>> ipa trustconfig-show
>>>>
>>>> ipa-adtrust-install --admin-password=jupiter123 --netbios-name=IPA -add-sids --enable-compat -U 
>>>>
>>>> echo -e "jupiter123\n" | kinit && echo "OK INIT"
>>>> echo -e "vagrant\n" | ipa trust-add --type=ad ad.edt.com --admin Administrator --password
>>>>
>>>> # Create users,groups  
>>>> bash users.sh
>>>>
>>>> ```

>>>> ```
>>>>bash recursos.sh
>>>> ```

>>> __INSTAL·LACIÓ PAQUETS__

>>>> *El primer pas a implementar es l'adquisico de recursos freeipa descarregantlo amb dnf totes les comandes relacionades amb freeipa.*

>>>> ```
>>>> dnf -y update
>>>> dnf -y install nmap tree net-tools freeipa-* bind bind-chroot
>>>> ```

>>>>> __Nota:__ *Per a solventar alguns futurs problemes hi ha que executar les seguents comandes relacionades amb la habilitacio i configuracio del IPv6 i seguretat SELinux.*

>>>> ```
>>>> cp /home/vagrant/config /etc/selinux/config
>>>> cp /home/vagrant/sysctl.conf /etc/sysctl.conf
>>>> sysctl -p
>>>> ```

>>>> __HOSTS__

>>>>> *Especifiquem els hosts per a una resolucio adecuada de noms amb aquesta comanda:.*

>>>>> ```
>>>>> cp /home/vagrant/hosts /etc/hosts
>>>>> ```

>>>> __INSTAL·LACIÓ FREEIPA__

>>>>> *Per a installar el servidor fa falta executar aquesta ordre on es te que especificar parametres importants com el domini ,el usuari administrador,real per kerberos i la configuracio de un servidor DNS.*

>>>>> ```
>>>>> ipa-server-install -a jupiter123 -p jupiter123 --domain=ipa.edt.edu --realm=IPA.EDT.EDU --hostname smb.ipa.edt.edu --ip-address=192.168.56.2 --mkhomedir --setup-dns --ssh-trust-dns --forwarder=8.8.8.8 --auto-reverse --no-host-dns -U
>>>>> ```

>>>>>> · __ipa-server-install:__ *Ordre de instalacio de servidor Freeipa.*

>>>>>>    - __-a jupiter123:__ *Especifica la contrasenya de l'administrador de l'IPA.*

>>>>>>    - __-p jupiter123:__ *Especifica la contrasenya del compte "admin" de l'IPA.*

>>>>>>    - __--domain=ipa.edt.edu:__ *Estableix el domini de l'IPA com "ipa.edt.edu".*
  
>>>>>>    - ___--realm=IPA.EDT.EDU:__ *Estableix el reialme de l'IPA com "IPA.EDT.EDU".*

>>>>>>    - __--hostname smb.ipa.edt.edu:__  *Especifica el nom de l'amfitrió (hostname) del servidor IPA com "smb.ipa.edt.edu".*

>>>>>>    - __--ip-address=192.168.56.2:__ *Especifica l'adreça IP del servidor IPA com "192.168.56.2".*
  
>>>>>>    - __--mkhomedir:__ *Habilita la creació automàtica de directoris d'inici per als usuaris en el sistema de fitxers.*

>>>>>>    - __--setup-dns:__ *Configura el servidor IPA per gestionar la resolució de noms DNS per al domini especificat.*
  
>>>>>>    - __--ssh-trust-dns:__  *Permet confiar en el registre DNS per resoldre els noms dels hosts quan s'estableixen connexions SSH.*

>>>>>>    - __--forwarder=8.8.8.8:__  *Especifica el servidor DNS de reenviament com "8.8.8.8". Aquest servidor DNS s'utilitzarà per resoldre els noms de domini que no es gestionen internament.*

>>>>>>    - __--auto-reverse:__ *Permet que el servidor IPA gestioni automàticament les zones de DNS inverses.*

>>>>>>    - __--no-host-dns:__  *Indica que el servidor IPA no s'utilitzarà com a servidor DNS per a l'amfitrió local.*

>>>>>>    - __-U:__  *S'utilitza per realitzar una instal·lació sense supervisió, evitant la sol·licitud de confirmació de l'usuari.*

>>>> __CONFIGURACIÓ FIREWALL__

>>>>> *Habilitem els ports per als serveis que composen freeipa i obrim manualment altre ports.*

>>>>> ```
>>>>> firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
>>>>> firewall-cmd --add-port={138,1024-1300,3268}/tcp --permanent
>>>>> firewall-cmd --add-port={123,138,53,139,445}/udp --permanent
>>>>> firewall-cmd --reload
>>>>> ```

>>>> __COMPROVACIÓ FUNCIONAMENT DNS__

>>>>> *Aquest pas es opcional ya que comprovem el funcionament del DNS.
Per aixo usem un ticket kerberos i utilitzem eines de ipa o el comand dig.
Per  ultim afegim una configuracio extra per a l'habilitacio IPv6 i reiniciem el servidor Frreipa.*

>>>>> ```
>>>>> echo -e "jupiter123\n" | kinit admin && echo "CHECK DNS OK" 
>>>>> dig smb.ipa.edt.edu
>>>>> ipa host-show smb.ipa.edt.edu
>>>>>
>>>>> cp /home/vagrant/ipa-options-ext.conf /etc/named/ipa-options-ext.conf && echo "COPY OK" 
>>>>> ipactl restart
>>>>> ```

>>>> __TRUST DOMINIS (MENCIONAT AMB DETALL EN L'APARTAT SAMBA)__

>>>>> *Aquesta part fa la relacio dels dominis.*

>>>>> ```
>>>>> # Trust AD
>>>>> yum install -y samba-*
>>>>> echo -e "jupiter123\n" | kinit && echo "OK TICKET AD"
>>>>> klist
>>>>> ipa dnsforwardzone-add ad.edt.com --forwarder=192.168.57.2 --forward-policy=only
>>>>> ipa trustconfig-show
>>>>>
>>>>> ipa-adtrust-install --admin-password=jupiter123 --netbios-name=IPA -add-sids --enable-compat -U 
>>>>>
>>>>> echo -e "jupiter123\n" | kinit && echo "OK INIT"
>>>>> echo -e "vagrant\n" | ipa trust-add --type=ad ad.edt.com --admin Administrator --password
>>>>> ```

>>>> __SCRIPTS D'ADMINISTRACIO I RECURSOS__

>>>>> *Per ultim llançem els cripts secundaris encarregats de crear els usuaris i grups (__users.sh__) i els fitxer per a compartir amb samba (__recursos.sh__).*

>>>>> ```
>>>>> # Create users,groups  
>>>>> bash users.sh
>>>>> ```

>>>>> ```
>>>>> bash recursos.sh
>>>>> ```

## 2. FASE 2: INTEGRACIÓ AMB SAMBA AD

> ### 2.1 INTRODUCCIÓ

>> · Instal·lació de SAMBA AD.

>> · Resolució DNS.

>> · Gestió d’usuaris.

>> · Integració de màquines al domini.

>> · Integració de sistemes GNU/Linux i Windows.

>>  La integració amb el servei de Samba4 AD ofereix una àmplia varietat de beneficis i funcionalitats en un entorn de xarxa. Alguns dels beneficis clau inclouen la gestió centralitzada d'identitats, la implementació d'un mecanisme d'autenticació única, la sincronització bidireccional d'usuaris i les contrasenyes, la compartició de polítiques d'accés i la interoperabilitat amb altres sistemes compatibles amb FreeIPA. 
 
>> La integració d'aquests sistemes heterogenis permet administrar usuaris i grups de manera més eficient i garanteix la coherència de les credencials i les polítiques de seguretat a tota la xarxa.

> ### 2.2 QUÈ ÉS SAMBA4 AD DC?

>> Samba4 AD DC és un programari de codi obert que permet crear un servidor de directori actiu en un entorn de xarxa. En termes simples, un servidor de directori actiu és una eina que organitza i emmagatzema informació sobre els usuaris, els ordinadors i altres recursos en una xarxa. Samba4 AD DC es basa en el projecte Samba i és compatible amb Active Directory de Microsoft. Això significa que es pot utilitzar Samba4 AD DC per crear i administrar usuaris, grups, permisos i altres recursos en una xarxa, de manera similar a com ho faries en un entorn Windows.

>> El gran avantatge de Samba4 AD DC és que és de codi obert, cosa que significa que és gratuït i pots modificar-lo segons les teves necessitats. A més, és compatible amb diferents sistemes operatius, cosa que et brinda flexibilitat per fer-lo servir en una varietat d'entorns de xarxa. En conclusió, Samba4 AD DC és una solució econòmica i flexible que us permet administrar usuaris i recursos en una xarxa de manera eficient i efectiva.

> ### 2.4 COM S'INTEGRA SAMBA4 AMB FREEIPA

>> El fet d'haver decidit fer l'instal·lació dels dos serveis dins una mateixa màquina, comporta certes dificultats afegides, entre elles possibles conflictes a l'hora de compartir ports i més.

>> Primerament s'instal·laen els paquests corresponents al samba.

>>> ```
>>> yum install -y samba-*
>>> echo -e "jupiter123\n" | kinit && echo "OK TICKET AD"
>>> klist
>>> ```
  
>> Desprès s'ha de configurar FreeIPA com a domini principal i establir una configuració correcta de DNS amb el domini de samba AD com a domini secundari o forwarder.

>>> ```
>>> ipa dnsforwardzone-add ad.edt.com --forwarder=192.168.57.2 --forward-policy=only
>>> ipa trustconfig-show
>>> ```

>> Ara s'ha d'establir la confiança entre entre el domini de FreeIPA i el domini d'Active Directory (AD) que vulguem integrar.

>>> ```
>>> ipa-adtrust-install --admin-password=jupiter123 --netbios-name=IPA -add-sids --enable-compat -U
>>> echo -e "jupiter123\n" | kinit && echo "OK INIT"
>>> echo -e "vagrant\n" | ipa trust-add --type=ad ad.edt.com --admin Administrator --password
>>  ```

>> També és important resaltar l'importància de kerberos en la relació de confiança entre dominis. Kerberos és essencial per garantir l'autenticació segura dels usuaris i protegir la comunicació entre FreeIPA i Active Directory. La seva integració amb Samba permet més interoperabilitat i seguretat en entorns mixtos de Linux i Windows. 
    
>> Kerberos i Samba són eines fonamentals per garantir la seguretat i l'eficiència a l'administració d'entorns de xarxa mixtos. Però la configuració própia de kerberos per garantir aquesta seguretat es fa automàticament en utilitzar les ordres ipa.

> ### 2.4 PROVES DE RESOLUCIÓ

>> Amb IP de màquina Windows amb recursos compartits i usuari adminitrador windows.

>>> ![win-recursos-smb](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/3%20TRIMESTRE/images/samba-recursos-normal.png)


>> Amb nom de domini i usuari creat dins del domini windows Vagrant. 

>>> ![win-recursos-smb-ad](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/3%20TRIMESTRE/images/samba-recursos-ad.png)

## 3. FASE 3: CREACIÓ DE RECURSOS I USUARIS

> ### 3.1 INTRODUCCIÓ

>> Creem el recursos compartits en la configuració del fitxer *__smb.conf__*. On assignem que compartim, permissos i quin grup d'usuaris tindràn accés.

> ### 3.2 RECURS ALUMNES

>> En aquest recurs anomenat __alumnes__ només tindràn accés tots els usuaris que pertanyin al grup de __alumnes__. En aquest grup i pertànyen els usuaris:

>>> · *pere*

>>> · *marta*

>>> · *anna*

>> ```
>> [alumnes]
>> comment = Documentació alumnes
>> path = /usr/share/notes
>> public = yes
>> browseable = yes
>> writable = no
>> printable = no
>> valid users = @alumnes, @admins, @profesors
>> ``` 

> ### 3.3 PROFESORS

>> En aquest recurs anomenat __alumnes__ només tindràn accés tots els usuaris que pertanyin al grup de __alumnes__. En aquest grup i pertànyen els usuaris:

>>> · *bob*

>>> · *alice*

>> ```
>> [profesors]
>> comment = Documentació de los profesors
>> path = /usr/share/examenes
>> public = yes
>> browseable = yes
>> writable = no
>> printable = no
>> valid users = @admins, @profesors
>> ``` 

> ### 3.4 ADMINS

>> En aquest recurs anomenat __admins__ només tindràn accés tots els usuaris que pertanyin al grup de __@admins__. En aquest grup i pertànyen els usuaris:

>>> · *mallory*

>> ```
>> [admins]
>> comment = Documentació sys admins 
>> path = /usr/share/fitxers
>> public = yes
>> browseable = yes
>> writable = no
>> printable = no
>> valid users = @admins
>> ```

> ### 3.5 DOCUMENTATION

>> ```
>> [documentation]
>> comment = Documentació doc del container
>> path = /usr/share/doc
>> public = yes
>> browseable = yes
>> writable = no
>> printable = no
>> guest ok = yes
>> ```

## 4. FASE 4: CONFIGURACIÓ DELS CLIENTS

> ### 4.1 CONFIGURACIÓ DEL CLIENT WINDOWS

>> El client windows creat per aquest laboratori és servidor AD i tambè membre del propi domini. No va ser possible crear una màquina membre i un altre de servidor per problemes amb la capacitat a l'hora de desplegar les màquines virtuals. Per tant per aquesta raó el client windows de aquest laboratori fa les dues funcions. 

>> Per la creació de la màquina windows s'ha utilitzat un fitxer vagrant i dos scripts de PowerShell.

>>> startup.ps1

>>> El primer script de PowerShell, startup.ps1, assigna un gateway a la màquina, a més de instal·lar els paquests neccessaris per crear un domini windows i finalment crea el domini anomenat ad.edt.com amb les configuracions corresponents.

>>>> ```
>>>> #IP estatica + gateway 
>>>> Get-NetAdapter
>>>> New-NetRoute -DestinationPrefix "0.0.0.0/0" -InterfaceAlias "Ethernet 2" -NextHop "192.168.57.1"
>>>> ipconfig
>>>>
>>>> #Get-windowsFeature
>>>> Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
>>>> Import-Module ADDSDeployment
>>>>
>>>> ipconfig
>>>>
>>>> #AD DS  + dominio 
>>>> Install-ADDSForest -DomainName "ad.edt.com" -DomainNetbiosName "EDT" -DomainMode "Win2012R2" -ForestMode "Win2012R2" -InstallDNS -SafeModeAdministratorPassword (ConvertTo-SecureString -String "jupiter123." -AsPlainText -Force) -Force
>>>> ```

>>> dns.ps1

>>> El segon script de PowerShell defineix el domini del FreeIPA en el propi servidor DNS del windows per poder lograr la connexió.

>>>> ```
>>>> dnscmd 127.0.0.1 /ZoneAdd ipa.edt.edu /Forwarder 192.168.56.2
>>>>Get-ADDomainController
>>>> ```

>>> Vagranfile

>>> Per establir la capacitat de la màquina (vb.memory, vb.cpu), quina eina de virtualització ha d'utilitzar (config.vm.provider), de quina imatge base es parteix (config.vm.box) i quin nom se li assigna a la màquina (config.vm.hostname).

>>>> ```
>>>> Vagrant.configure("2") do |config|
>>>>  config.vm.box = "gusztavvargadr/windows-server"
>>>>  config.vm.provider "virtualbox" do |vb|
>>>>    vb.memory = "2000"
>>>>    vb.cpus = "2"
>>>>  end
>>>>  config.vm.hostname = "windows"
>>>> ```

>>> Per evitar conflictes per temes de seguretat, s'habilita el tràfic en text pla i autenticació bàsica.

>>>> ```
>>>> config.winrm.transport = :plaintext
>>>> config.winrm.basic_auth_only = true
>>>> ```

>>> Per assignar una ip estàtica a la màquina.

>>>> ```
>>>> config.vm.network "private_network", ip: "192.168.57.2"
>>>> ```

>>> Per executar el script d'aprovisionament inicial startup.ps1.

>>>> ```
>>>> config.vm.provision "shell",powershell_elevated_interactive: "true",path: "./startup.ps1", args: "-ExecutionPolicy Bypass"
>>>> ```

>>> Per executar el segon script dns.ps1

>>>> ```
>>>> config.vm.provision "shell", name: "dns", inline: "powershell -ExecutionPolicy Bypass -File \"C:/vagrant/dns.ps1\" -Username 'Administrator' -Password 'vagrant'", privileged: true
>>>> ```

>>> El fitxer vagrant final es veu de la seguent manera:

>>>> ```
>>>> # -*- mode: ruby -*-
>>>> # vi: set ft=ruby :
>>>>
>>>> Vagrant.configure("2") do |config|
>>>>  config.vm.box = "gusztavvargadr/windows-server"
>>>>  config.vm.provider "virtualbox" do |vb|
>>>>    vb.memory = "2000"
>>>>    vb.cpus = "2"
>>>>  end
>>>>  config.vm.hostname = "windows"
>>>>  # use the plaintext WinRM transport and force it to use basic authentication.
>>>>  # NB this is needed because the default negotiate transport stops working
>>>>  #    after the domain controller is installed.
>>>>  #    see https://groups.google.com/forum/#!topic/vagrant-up/sZantuCM0q4
>>>>  config.winrm.transport = :plaintext
>>>>  config.winrm.basic_auth_only = true
>>>>
>>>>  config.vm.network "private_network", ip: "192.168.57.2"
>>>>  config.vm.provision "shell",powershell_elevated_interactive: "true",path: "./startup.ps1", args: "-ExecutionPolicy Bypass"
>>>>  #config.vm.provision "reload"
>>>>  #config.vm.provision "shell", name:"dns", powershell_elevated_interactive: "true",path: "./dns.ps1", privileged: "true"
>>>>  config.vm.provision "shell", name: "dns", inline: "powershell -ExecutionPolicy Bypass -File \"C:/vagrant/dns.ps1\" -Username 'Administrator' -Password 'vagrant'", privileged: true
>>>>
>>>> end
>>>> ```

> ### 4.2 CONFIGURACIÓ CLIENT CENTOS

>>Freeipa ofereix una rapida implementacio de un client linux al seu servidor amb poques comandes i en pocs minuts. 

>>### 4.2.1 VAGRANTFILE

>>>```
>>># vi: set ft=ruby :
>>>
>>># Maquina Freipa servidor
>>>
>>>Vagrant.configure("2") do |config|
>>>  config.vm.define "centos8" do |server|
>>>    server.vm.box = "generic/centos8"
>>>    server.vm.provider "virtualbox" do |vb|
>>>      vb.memory = "2000"
>>>      vb.cpus = "2"
>>>    end
>>>    server.vm.hostname = "centosclie.ipa.edt.edu"
>>>    server.vm.network "private_network", ip: "192.168.56.4"
>>>    server.vm.provision "file", source: "./files/resolv.conf", destination: "/$HOME/"
>>>    server.vm.provision "file", source: "./files/hosts", destination: "/$HOME/"
>>>    server.vm.provision "shell", path: "./startup.sh"
>>>  end
>>>end
>>>```

>>> En aquest apartat especifiquem els requeriments basics que tindra la nostra maquina virtual.

>>>  · __Nom secció de configuració:__ *Server.*

>>>  · __Imatge utilitzada:__ *S'utilitzara la imatge **generic/centos8**.*

>>>  · __Nom del proveidor:__ *Virtualbox.*

>>>  · __Memoria:__ *2000 Mib.*

>>>  · __Numero de CPUs:__ *2.*

>>>  · __Nom de la maquina:__ *centosclie.ipa.edt.edu*

>>>  · __IP dins la xarxa:__ *192.168.56.4*

>>>  . __Fichers proveits__ *Es proveix la maquina amb els fitxers resolv.conf i hosts*

>>>  . __Script principal__ *Cuan comenci la instancia s'executara el fitxer startup.sh*


>> ### 4.2.2 Fichers de configuracio

>>> Per a poder treballar adecuadment hi ha que proporcionar a la maquina virtual un ficher de resolucio de noms i definir el servidor dns 

>>> __hosts__
>>>>*Fitxers que conte la resolucio de noms i associacio de nom amb IP*
>>>>```
>>>>127.0.1.1 centos.edt.edu centos
>>>>192.168.56.2 smb.ipa.edt.edu smb
>>>>192.168.57.2 windows.ad.edt.com windows
>>>>```
>>>> · __127.0.1.1 centos.edt.edu centos:__ *Resolució de nom per al loopback.*

>>>> · __192.168.56.2 smb.ipa.edt.edu smb:__ *Resolució de nom per al servidor Freeipa amb hostname **smb.ipa.edt.edu** o **smb** a la IP **192.168.56.2**

>>>> · __192.168.57.2  windows.ad.edt.com :__ *Resolució de nom per al Windows server amb hostname **windows.ad.edt.com** o  **windows** per a la IP **192.168.57.2**.*

>>> __resolv.conf__
>>>> En aquest fitxer definim el servidor dns que utilitzara el nostre client
>>>>```
>>>># Generated by NetworkManager
>>>>search edt.edu
>>>>#nameserver 10.0.2.3
>>>>nameserver 192.168.56.2
>>>>options single-request-reopen
>>>>options single-request-reopen
>>>>options single-request-reopen
>>>>```

>>>>  . __search edt.edu:__ *Definim el domini que te que buscar (__edt.edu__)*
 
>>>>  .  __nameserver 192.168.56.2:__ *El servidor dns de la maquina es el servidor Freipa*

>> ### 4.2.3 STARTUP.SH

>>>El fitxer startup.sh instala el client freeipa i configura el firewall per a un correcte funcionament

>>>>```
>>>>#! /bin/bash
>>>>dnf update -y
>>>>
>>>>cp /home/vagrant/hosts /etc/hosts
>>>>cp /home/vagrant/resolv.conf /etc/resolv.conf
>>>>
>>>>dnf -y install ipa-client
>>>>dnf -y install ipa-client-samba
>>>>ipa-client-install --unattended --mkhomedir --principal admin --password=jupiter123
>>>>ipa-client-samba --unattended
>>>>
>>>>
>>>>firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
>>>>#sudo firewall-cmd --add-port=137-138/udp --permanent
>>>>#sudo firewall-cmd --add-port={139,445}/tcp --permanent
>>>>firewall-cmd --reload
>>>>
>>>>systemctl restart smb
>>>>systemctl restart winbind
>>>>
>>>>```
>>>>Copiem els ficher de configuracio cadascun a la seva ruta
>>>>```
>>>>cp /home/vagrant/hosts /etc/hosts
>>>>cp /home/vagrant/resolv.conf /etc/resolv.conf
>>>>```
>>>Instalem els paquets necesaris per freeipa i samba  al client amb:
>>>>```
>>>>dnf update -y
>>>>dnf -y install ipa-client
>>>>dnf -y install ipa-client-samba
>>>>```
>>>>Habilitem els port necesaris per las serveis de freeipa , habilitem altres de forma manual i apliquem els canvis
>>>>```
>>>>firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
>>>>#sudo firewall-cmd --add-port=137-138/udp --permanent
>>>>#sudo firewall-cmd --add-port={139,445}/tcp --permanent
>>>>firewall-cmd --reload
>>>>```
>>>>Per ultim reiniciem els serveis relacionats amb samba
>>>>```
>>>>systemctl restart smb
>>>>systemctl restart winbind
>>>>```
## BIBLIOGRAFÍA

> La bibliografia és una part essencial d'un treball acadèmic o de recerca, ja que proporciona informació sobre les fonts utilitzades per a la seva realització. 

> En aquesta secció, es detallen les fonts bibliogràfiques emprades per a la realització d'aquest treball:

>> <https://www.lpi.org/blog/2021/09/28/lpic-3-mixed-environments-30-introduction-05-305-linux-identity-management-and-file>

>> <https://www.youtube.com/watch?v=ug61zd9emc0&t=1285s>

>> <https://www.youtube.com/watch?v=1sVcbofs2r8>

>> <https://red-orbita.com/?p=7563>

>> <https://administradordesistema.files.wordpress.com/2018/08/freeipa.pdf>

>> <https://github.com/rgl/windows-domain-controller-vagrant/blob/master/Vagrantfile>

>> <https://fedoraproject.org/wiki/QA:Testcase_freeipa_trust_server_installation>

>> <https://fedoraproject.org/wiki/QA:Testcase_freeipa_prepare_server_for_trusts>

>> <https://fedoraproject.org/wiki/FreeIPA_step_by_step >

>> <https://www.linuxsysadmins.com/integrating-idm-with-ad-cross-forest-trust/ >

>> <https://www.linuxsysadmins.com/identity-management-server-in-rhel-with-ipa/ >

>> <https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/7/html/windows_integration_guide/active-directory-trust>

>> <https://raduzaharia.medium.com/adding-users-and-shares-to-freeipa-1f0a2c4785 
>