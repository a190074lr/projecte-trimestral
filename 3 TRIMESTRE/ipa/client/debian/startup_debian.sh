#! /bin/bash
hostnamectl set-hostname debian1.edt.edu
apt-get update
DEBIAN_FRONTEND=noninteractive  apt-get install -y  freeipa-client 

# dns files 
cp /home/vagrant/hosts_debian /etc/hosts
cp /home/vagrant/resolv_debian /etc/resolv.conf

# ipa client
sudo ipa-client-install --unattended --mkhomedir --principal admin --password=jupiter123

# config firewall
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 123 -j ACCEPT
iptables -A INPUT -p tcp --dport 389 -j ACCEPT
iptables -A INPUT -p tcp --dport 636 -j ACCEPT

mkdir /etc/iptables
touch /etc/iptables/rules.v4
iptables-save >> /etc/iptables/rules.v4

iptables-restore < /etc/iptables/rules.v4
