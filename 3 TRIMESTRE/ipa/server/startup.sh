#! /bin/bash

dnf -y update
dnf -y install nmap tree net-tools freeipa-* bind bind-chroot

cp /home/vagrant/config /etc/selinux/config
cp /home/vagrant/sysctl.conf /etc/sysctl.conf
cp /home/vagrant/hosts /etc/hosts
sysctl -p

# Activate FreeIpa server
ipa-server-install -a jupiter123 -p jupiter123 --domain=ipa.edt.edu --realm=IPA.EDT.EDU --hostname smb.ipa.edt.edu --ip-address=192.168.56.2 --mkhomedir --setup-dns --ssh-trust-dns --forwarder=8.8.8.8 --auto-reverse --no-host-dns -U

# Config firewall
firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
firewall-cmd --add-port={138,1024-1300,3268}/tcp --permanent
firewall-cmd --add-port={123,138,53,139,445}/udp --permanent
firewall-cmd --reload

# Check dns service 
echo -e "jupiter123\n" | kinit admin && echo "CHECK DNS OK" 
dig smb.ipa.edt.edu
ipa host-show smb.ipa.edt.edu

cp /home/vagrant/ipa-options-ext.conf /etc/named/ipa-options-ext.conf && echo "COPY OK" 
ipactl restart

# Trust AD
yum install -y samba-*
echo -e "jupiter123\n" | kinit && echo "OK TICKET AD"
klist
ipa dnsforwardzone-add ad.edt.com --forwarder=192.168.57.2 --forward-policy=only
ipa trustconfig-show

ipa-adtrust-install --admin-password=jupiter123 --netbios-name=IPA -add-sids --enable-compat -U 

echo -e "jupiter123\n" | kinit && echo "OK INIT"
echo -e "vagrant\n" | ipa trust-add --type=ad ad.edt.com --admin Administrator --password

# Create users,groups  
bash users.sh

#
bash recursos.sh
